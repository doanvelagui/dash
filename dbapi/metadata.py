import sqlalchemy
import traceback

def test():
    return "sqlalchemy v%s" % sqlalchemy.__version__

def error_info(s=None, t=None, u=None, h=None):
    return {'source':s, 'traceback':t, 'uri':u, 'huminize':h}

def get_engine(uri='sqlite:///:memory:'):
    engine = None
    try:
        return sqlalchemy.create_engine(uri)
    except:
        return error_info(s='create_engine', t=traceback.format_exc(), u=uri, h='while trying to connect to database')

def get_inspect(engine):
    try:
        return sqlalchemy.inspect(engine)
    except:
        return error_info(s='create_engine', t=traceback.format_exc(), u=uri, h='while trying to connect to database')

def check_connection(uri='sqlite:///:memory:'):
    data = {}
    engine = get_engine(uri)
    if isinstance(engine, dict):
        # means error on connection
        data['errors'] = [engine]
        engine = None
    if engine:
        # means no error found
        inspector = get_inspect(engine)
        if isinstance(inspector, dict):
            data['errors'] = [inspector]
    # TODO is it good to return True/error, or shoud include a 'status' value on dict and return it
    return data if data else True

def metadata(uri='sqlite:///:memory:'):
    data = {}
    engine = get_engine(uri)
    if isinstance(engine, dict):
        # means error on connection
        data['errors'] = [engine]
        engine = None
    if engine:
        # means no error found
        inspector = get_inspect(engine)
        if isinstance(inspector, dict):
            data['errors'] = [inspector]
        else:
            data['tables'] = {}
            for i in inspector.get_table_names():
                t = {'name':i, 'column':{}}
                for j in inspector.get_columns(i):
                    t['column'][j['name']] = j.copy()
                    # TODO normalize types: str_types(t['column'][j['name']]['type']). For now just cast to str
                    t['column'][j['name']]['type'] = str(type(t['column'][j['name']]['type']))
                for j in inspector.get_foreign_keys(i):
                    multiple = len(j['constrained_columns'])>1
                    for k in j['constrained_columns']:
                        t['column'][k]['foreign_key'] = j.copy()
                        t['column'][k]['foreign_key']['multiple'] = multiple
                data['tables'][i] = t
    return data
