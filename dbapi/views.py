#from django.shortcuts import render
from django.shortcuts import render_to_response
from .metadata import test

def home(request):
    # import sqlalchemy
    version = test()
    return render_to_response('dbapi/home.html', locals())