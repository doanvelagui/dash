from django.db import models

class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

class Role(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return self.name

class Enterprise(models.Model):
    name = models.CharField(max_length=50)
    subdomain = models.CharField(max_length=10)
    def __str__(self):
        return self.name

class Team(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    enterprise = models.ForeignKey('Enterprise', on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Invite(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    enterprise = models.ForeignKey('Enterprise', on_delete=models.CASCADE)
    def __str__(self):
        return "%(name)s (%(email)s)" % dict(self)

class User_Role(models.Model):
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    enterprise = models.ForeignKey('Enterprise', on_delete=models.CASCADE)
    # def __str__(self):
    #     return "%(user)s %(role)s %(enterprise)s" % dict(self)
    class Meta:
        unique_together = ('user', 'enterprise', 'role')

class User_Role_Team(models.Model):
    user_role = models.ForeignKey('User_Role', on_delete=models.CASCADE)
    team = models.ForeignKey('Team', on_delete=models.CASCADE)
    # def __str__(self):
